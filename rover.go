package main

import (
	"fmt"
	"image"
	"math/rand"
	"sync"
	"time"
)

type marsGrid struct {
	mu     sync.Mutex
	bounds image.Rectangle
	cells  [][]cell
}

type cell struct {
	groundData sensorData
	occupier   *occupier
}

type occupier struct {
	grid *marsGrid
	pos  image.Point
}

type sensorData struct {
	lifeSigns int
}

type message struct {
	pos       image.Point
	lifeSigns int
	rover     string
}

type radio struct {
	fromRover chan message
}

type roverDriver struct {
	commandc chan command
	occupier *occupier
	name     string
	radio    *radio
}

type command int

const (
	right = command(0)
	left  = command(1)

	dayLength         = 24 * time.Second
	receiveTimePerDay = 2 * time.Second
)

func main() {
	marsToEarth := make(chan []message)
	go earthReceiver(marsToEarth)

	gridSize := image.Point{X: 20, Y: 10}
	grid := newMarsGrid(gridSize)

	grid.print()

	rovers := make([]*roverDriver, 5)

	for i := range rovers {
		rovers[i] = startDriver(fmt.Sprint("rover", i), grid, marsToEarth)
	}

	time.Sleep(60 * time.Second)
}

func newMarsGrid(size image.Point) *marsGrid {
	grid := &marsGrid{
		bounds: image.Rectangle{Max: size},
		cells:  make([][]cell, size.Y),
	}

	for y := range grid.cells {
		grid.cells[y] = make([]cell, size.X)
		for x := range grid.cells[y] {
			cell := &grid.cells[y][x]
			cell.groundData.lifeSigns = rand.Intn(1000)
		}
	}
	return grid
}

func newRoverDriver(name string, occupier *occupier, marsToEarth chan []message) *roverDriver {
	r := &roverDriver{
		commandc: make(chan command),
		occupier: occupier,
		name:     name,
		radio:    newRadio(marsToEarth),
	}
	go r.drive()
	return r
}

func newRadio(toEarth chan []message) *radio {
	r := &radio{fromRover: make(chan message)}

	go r.run(toEarth)
	return r
}

func startDriver(name string, grid *marsGrid, marsToEarth chan []message) *roverDriver {
	var o *occupier
	for o == nil {
		startPoint := image.Point{X: rand.Intn(grid.size().X), Y: rand.Intn(grid.size().Y)}
		o = grid.occupy(startPoint)
	}
	return newRoverDriver(name, o, marsToEarth)
}

func (r *roverDriver) drive() {
	fmt.Printf("%s initial position %v\n", r.name, r.occupier.pos)
	direction := image.Point{X: 1, Y: 0}
	updateInterval := 250 * time.Millisecond
	nextMove := time.After(updateInterval)
	for {
		select {
		case c := <-r.commandc:
			switch c {
			case right:
				direction = image.Point{X: -direction.Y, Y: direction.X}
				fmt.Println("heading right")
			case left:
				direction = image.Point{X: direction.Y, Y: -direction.X}
				fmt.Println("heading left")
			}
		case <-nextMove:
			nextMove = time.After(updateInterval)
			newPos := r.occupier.pos.Add(direction)
			if r.occupier.moveTo(newPos) {
				fmt.Printf("%s moved to %v\n", r.name, newPos)
				r.checkForLife()
				break
			}
			fmt.Printf("%s blocked trying to move from %v to %v\n", r.name, r.occupier.pos, newPos)
			dir := rand.Intn(3) + 1
			for i := 0; i < dir; i++ {
				direction = image.Point{X: -direction.Y, Y: direction.X}
			}
			fmt.Printf("%s new random direction %v\n", r.name, direction)
		}
	}
}

func (r *roverDriver) left() {
	r.commandc <- left
}

func (r *roverDriver) right() {
	r.commandc <- right
}

func (g *marsGrid) occupy(p image.Point) *occupier {
	g.mu.Lock()
	defer g.mu.Unlock()

	cell := g.cell(p)
	if cell == nil || cell.occupier != nil {
		return nil
	}
	cell.occupier = &occupier{grid: g, pos: p}

	return cell.occupier
}

func (o *occupier) moveTo(p image.Point) bool {
	o.grid.mu.Lock()
	defer o.grid.mu.Unlock()

	newCell := o.grid.cell(p)
	if newCell == nil || newCell.occupier != nil {
		return false
	}

	o.grid.cell(o.pos).occupier = nil
	newCell.occupier = o
	o.pos = p
	return true
}

func (o *occupier) sense() sensorData {
	o.grid.mu.Lock()
	defer o.grid.mu.Unlock()

	return o.grid.cell(o.pos).groundData
}

func (g *marsGrid) size() image.Point {
	return g.bounds.Max
}

func (g *marsGrid) cell(p image.Point) *cell {
	if !p.In(g.bounds) {
		return nil
	}
	return &g.cells[p.Y][p.X]
}

func (g *marsGrid) print() {
	const line = "----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
	fmt.Println(line)
	for _, row := range g.cells {
		for _, col := range row {
			fmt.Printf("| %v |", col)
		}
		fmt.Println()
		fmt.Println(line)
	}
}

func (r *roverDriver) checkForLife() {
	sensorData := r.occupier.sense()
	if sensorData.lifeSigns < 900 {
		return
	}
	r.radio.sendToEarth(message{pos: r.occupier.pos, lifeSigns: sensorData.lifeSigns, rover: r.name})
}

func (r *radio) sendToEarth(m message) {
	r.fromRover <- m
}

func (r *radio) run(toEarth chan []message) {
	var buffered []message
	for {
		toEarth1 := toEarth
		if len(buffered) == 0 {
			toEarth1 = nil
		}
		select {
		case m := <-r.fromRover:
			buffered = append(buffered, m)
		case toEarth1 <- buffered:
			buffered = nil
		}
	}
}

func earthReceiver(msgc chan []message) {
	for {
		time.Sleep(dayLength - receiveTimePerDay)
		receiveMarsMessages(msgc)
	}
}

func receiveMarsMessages(msgc chan []message) {
	finished := time.After(receiveTimePerDay)
	for {
		select {
		case <-finished:
			return
		case ms := <-msgc:
			for _, m := range ms {
				fmt.Printf("earth received report of life sign level %d from %s at %v\n", m.lifeSigns, m.rover, m.pos)
			}

		}
	}
}
